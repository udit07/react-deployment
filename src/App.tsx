import React from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";

import LoginPage from "./container/login";
import RegisterPage from "./container/register";
import AuthenticatedRoutes from "./authenticatedRoutes";
import Home from "./container/home-page";

const SERVER_URL = `http://localhost:3001/graphql`;

const client = new ApolloClient({
  uri: SERVER_URL,
  request: operation => {
    const token = localStorage.getItem("token");
    operation.setContext({
      headers: {
        authorization: token ? token : ""
      }
    });
  }
});

function App() {
  return (
    <ApolloProvider client={client}>
      <Router>
        <Switch>
          <Route path="/login" component={LoginPage} />
          <Route path="/register" component={RegisterPage} />
          <Route>
            <AuthenticatedRoutes>
              <Home />
            </AuthenticatedRoutes>
          </Route>
        </Switch>
      </Router>
    </ApolloProvider>
  );
}

export default App;
