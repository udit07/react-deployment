import React from "react";

import { Redirect } from "react-router-dom";

const AuthenticatedRoutes = (props: { children: any }) => {
  const token = window.localStorage.getItem("token");

  return !token ? <Redirect to="/login" /> : props.children;
};

export default AuthenticatedRoutes;
