import React from "react";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import CustomerForm from "./container/customer/form";
import CustomerList from "./container/customer/list";

const AppRoutes = () => (
  <Switch>
    <Route path="/" exact>
      <CustomerList />
    </Route>
    <Route exact path="/customer-form">
      <CustomerForm />
    </Route>
    <Route path="/customer-form/:customerId">
      <CustomerForm />
    </Route>
  </Switch>
);

export default AppRoutes;
