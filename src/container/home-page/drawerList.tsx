import React, { FunctionComponent } from "react";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import PersonIcon from "@material-ui/icons/Person";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import Divider from "@material-ui/core/Divider";
import { useHistory } from "react-router-dom";

export const DrawerList: FunctionComponent<{ toolbar: any }> = ({
  toolbar
}) => {
  const history = useHistory();
  return (
    <div>
      <div className={toolbar} />
      <Divider />
      <List>
        <ListItem button onClick={() => history.push("/")}>
          <ListItemIcon>{<PersonIcon />}</ListItemIcon>
          <ListItemText primary={`Customer`} />
        </ListItem>
        <Divider />
        <ListItem
          button
          onClick={() => {
            window.localStorage.removeItem("token");
            history.push("/login");
          }}
        >
          <ListItemIcon>{<ExitToAppIcon />}</ListItemIcon>
          <ListItemText primary={`Log out`} />
        </ListItem>
      </List>
    </div>
  );
};
