import React, { useEffect } from "react";

import { Formik } from "formik";
import { useParams, useHistory } from "react-router";
import { Button, TextField, Paper, Grid, Typography } from "@material-ui/core";
import { useMutation, useLazyQuery } from "@apollo/react-hooks";
import { get as _get } from "lodash";

import {
  CREATE_CUSTOMER,
  GET_BY_CUSTOMER_ID,
  UPDATE_CUSTOMER
} from "../../state/customer/action";
import { useStyles } from "./style";

const CustomerForm = () => {
  const [addCustomer] = useMutation(CREATE_CUSTOMER);
  const [updateCustomer] = useMutation(UPDATE_CUSTOMER);
  const [getCustomer, { data }] = useLazyQuery(GET_BY_CUSTOMER_ID);
  const classes = useStyles();
  const history = useHistory();
  const { customerId } = useParams();

  useEffect(() => {
    if (customerId) {
      getCustomer({
        variables: { id: customerId }
      });
    }
  }, []);

  return (
    <Formik
      initialValues={{
        first_name: _get(data, "getCustomer.first_name", ""),
        last_name: _get(data, "getCustomer.last_name", ""),
        address: _get(data, "getCustomer.address", "")
      }}
      onSubmit={({ first_name, last_name, address }) => {
        if (customerId) {
          updateCustomer({
            variables: { id: customerId, first_name, last_name, address }
          })
            .then(res => console.log("customer updated successfully", res))
            .catch(err => console.log("failed to update customer", err));
        } else {
          addCustomer({ variables: { first_name, last_name, address } })
            .then(res => console.log("customer created successfully", res))
            .catch(err => console.log("failed to create customer", err));
        }
        history.push("/");
      }}
      enableReinitialize
    >
      {formik => (
        <Paper className={classes.paper}>
          <Typography color="primary" variant="h5" gutterBottom>
            Customer Form
          </Typography>
          <form onSubmit={formik.handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  label="First Name"
                  name="first_name"
                  onChange={formik.handleChange}
                  value={formik.values.first_name}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  label="Last Name"
                  name="last_name"
                  onChange={formik.handleChange}
                  value={formik.values.last_name}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  label="Address"
                  name="address"
                  onChange={formik.handleChange}
                  value={formik.values.address}
                />
              </Grid>
            </Grid>
            <Button
              className={classes.submit}
              variant="contained"
              color="primary"
              type="submit"
            >
              Submit
            </Button>
          </form>
        </Paper>
      )}
    </Formik>
  );
};

export default CustomerForm;
