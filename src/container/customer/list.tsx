import React, { useEffect, Fragment } from "react";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Button } from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import Typography from "@material-ui/core/Typography";

import { useHistory } from "react-router-dom";
import { useMutation, useQuery } from "@apollo/react-hooks";

import { useStyles } from "./style";
import { GET_ALL_CUSTOMER, REMOVE_CUSTOMER } from "../../state/customer/action";

const CustomerList = () => {
  const { data, refetch } = useQuery(GET_ALL_CUSTOMER);
  const [deleteCustomer] = useMutation(REMOVE_CUSTOMER);
  const classes = useStyles();
  const history = useHistory();

  useEffect(() => {
    // if (data) {
    refetch();
    console.log("component mount successfully", data);
    // }
  }, []);

  return (
    <Fragment>
      <Paper className={classes.paper}>
        <Typography color="primary" variant="h5" gutterBottom>
          Customer List
        </Typography>
        <Button
          variant="contained"
          color="primary"
          onClick={() => history.push("/customer-form")}
          className={classes.button}
        >
          Add New Customer
        </Button>

        <TableContainer>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>First Name</TableCell>
                <TableCell>Last Name</TableCell>
                <TableCell>Address</TableCell>
                <TableCell>Edit</TableCell>
                <TableCell>Delete</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data &&
                data.getCustomers.map((customer: any, key: any) => (
                  <TableRow key={key}>
                    <TableCell component="th" scope="row">
                      {customer.first_name}
                    </TableCell>
                    <TableCell>{customer.last_name}</TableCell>
                    <TableCell>{customer.address}</TableCell>
                    <TableCell>
                      <EditIcon
                        onClick={() =>
                          history.push(`customer-form/${customer.id}`)
                        }
                      />
                    </TableCell>
                    <TableCell>
                      <DeleteIcon
                        onClick={() => {
                          deleteCustomer({ variables: { id: customer.id } })
                            .then(res => {
                              console.log("customer removed successfully", res);
                              refetch();
                            })
                            .catch(err =>
                              console.log("failed to remove customer", err)
                            );
                        }}
                      />
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </Fragment>
  );
};

export default CustomerList;
