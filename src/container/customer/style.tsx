import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  table: {
    minWidth: 650
  },
  paper: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2)
  },
  button: {
    float: "right",
    marginRight: theme.spacing(3),
    marginBottom: theme.spacing(2)
  },
  submit: {
    marginTop: theme.spacing(2)
  }
}));
