import gql from "graphql-tag";

export const CREATE_CUSTOMER = gql`
  mutation AddCustomer(
    $first_name: String!
    $last_name: String!
    $address: String!
  ) {
    addCustomer(
      first_name: $first_name
      last_name: $last_name
      address: $address
    ) {
      id
    }
  }
`;

export const UPDATE_CUSTOMER = gql`
  mutation UpdateCustomer(
    $id: ID!
    $first_name: String!
    $last_name: String!
    $address: String!
  ) {
    updateCustomer(
      id: $id
      first_name: $first_name
      last_name: $last_name
      address: $address
    ) {
      id
    }
  }
`;

export const GET_ALL_CUSTOMER = gql`
  {
    getCustomers {
      id
      first_name
      last_name
      address
    }
  }
`;

export const REMOVE_CUSTOMER = gql`
  mutation DeleteCustomer($id: ID!) {
    deleteCustomer(id: $id) {
      id
    }
  }
`;

export const GET_BY_CUSTOMER_ID = gql`
  query Customer($id: ID!) {
    getCustomer(id: $id) {
      id
      first_name
      last_name
      address
    }
  }
`;
